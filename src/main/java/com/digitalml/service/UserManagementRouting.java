package com.digitalml.service;

import static spark.Spark.*;
import spark.*;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.MultipartBody;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Context.Builder;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.context.JavaBeanValueResolver;
import com.github.jknack.handlebars.context.MapValueResolver;

import static net.logstash.logback.argument.StructuredArguments.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class UserManagementRouting {

    private static final Logger logger = LoggerFactory.getLogger("usermanagement:6.0.0.0.0.0-SNAPSHOT");

    private static final String cacheURL = "http://infocache-test:9200/cache";
    
    private static final String resourceName = "usermanagement";

    public static void main(String[] args) {
   
   		// Initialise index on startup
		Unirest.put(cacheURL).asStringAsync();
   
        port(4567);
    
        get("/ping", (req, res) -> {
            return "pong";
        });
        
        get("/halt", (request, response) -> {
			stop();
			response.status(202);
			return "";
		});
		
        // Handle timings
        
        Map<Object, Long> timings = new ConcurrentHashMap<>();
        
        before(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.put(request, System.nanoTime());
        	}
        });
        
        after(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		long start = timings.remove(request);
        		long end =  System.nanoTime();
        		logger.info("log message {} {} {} {} ns", value("apiname", "usermanagement"), value("apiversion", "6.0.0.0.0.0-SNAPSHOT"), value("apipath", request.pathInfo()), value("response-timing", (end-start)));
        	}
        });
        
        afterAfter(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.remove(request);
        	}
        });

        post("/manage/user/inquiry/dataService", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/dataServices", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/inquiry/privilege/account/summary", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/function/privileges", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/inquiry/privilege/function", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/inquiry/privilege/account/detail", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/inquiry/summary", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/create", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/modify", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/approve", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/account/privileges", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/remove", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/inquiry/detail", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/manage/user/reject", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
    }
}